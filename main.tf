terraform {
  required_providers {
    fortimanager = {
      source = "fortinetdev/fortimanager"
      version = "1.3.5"
    }
  }
  backend "local" {
    path = "state/terraform.tfstate"
  }
}

provider "fortimanager" {
  hostname     = "${var.hostname}"
  username     = "${var.username}"
  password     = "${var.password}"
  insecure     = "true"
  scopetype    = "adom"
  adom         = "${var.adom_name}"
  logsession   = true
  presession   = "${var.presession}"
}


resource "fortimanager_exec_workspace_action" "lockres" { 
  scopetype      = "inherit"
  action         = "lockbegin"
  target         = ""
  param          = ""
  force_recreate = uuid()
  comment        = ""
}

resource "fortimanager_object_firewall_address" "member_server" {
  for_each              = "${var.member_server}"
  name                  = "${each.key}"
  type                  = "iprange"
  start_ip              = "${each.value.start_ip}"
  end_ip                = "${each.value.end_ip}"
  associated_interface  = "any"
  color                 = 7
  depends_on            = [fortimanager_exec_workspace_action.lockres]
}

resource "fortimanager_object_firewall_address" "member_client" {
  for_each              = "${var.member_client}"
  name                  = "${each.key}"
  type                  = "iprange"
  start_ip              = "${each.value.start_ip}"
  end_ip                = "${each.value.end_ip}"
  associated_interface  = "any"
  color                 = 7
  depends_on            = [fortimanager_exec_workspace_action.lockres]
}

# group_server
resource "fortimanager_object_firewall_addrgrp" "group_server" {
  allow_routing = "disable"
  member        = [for i,v in var.member_server : "${i}"]
  name          = "${var.group_server}"
  color         = 7
  depends_on = [fortimanager_object_firewall_address.member_server]
}

# member_client
resource "fortimanager_object_firewall_addrgrp" "group_client" {
  allow_routing = "disable"
  member        = [for i,v in var.member_client : "${i}"]
  name          = "${var.group_client}"
  color         = 7
  depends_on    = [fortimanager_object_firewall_address.member_client]
}


resource "fortimanager_exec_workspace_action" "unlockres" { # save change and unlock root ADOM
  scopetype      = "inherit"
  action         = "lockend"
  target         = ""
  param          = ""
  force_recreate = uuid()
  comment        = ""
  depends_on     = [
    fortimanager_object_firewall_address.member_server,
    fortimanager_object_firewall_address.member_client,
    fortimanager_object_firewall_addrgrp.group_server,
    fortimanager_object_firewall_addrgrp.group_client
  ]
}