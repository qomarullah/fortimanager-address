group_server = "group_nomadserverdtbs"
group_client = "group_nomadclientdtbs"

member_server = {
    member_nomadserverdtbs1 = {
        start_ip =  "10.52.76.1"
        end_ip =  "10.52.76.6"
    }
}

member_client = {
    member_nomadclientdtbs1 = {
        start_ip =  "10.52.76.7"
        end_ip =  "10.52.76.12"
    }
}
