# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/fortinetdev/fortimanager" {
  version     = "1.3.5"
  constraints = "1.3.5"
  hashes = [
    "h1:QhKYreCLTzV+Xk/wtmH5Ux8MPDoP9ZzgDzG3r3RXRtw=",
    "zh:18d7a3d12ea8e0202f98de49ca8499a79e8e98f7bb804db1ba8601bdc1fa8260",
    "zh:1a0e3152a108e17627cad365ee68fffb647f82352982ec85a886ff76c8b5adb8",
    "zh:5d8cb4e5de183ea78a225f9396b7591f18360d3c5e246b2e7ec41194beadc627",
    "zh:a38e28a99cd4e1a064ecefb92c1b7a57e9399d2476387ca5b4ea94a504ae5fc2",
    "zh:a476973265c42e808b4fbcaf4a8a50125a573c7e256b22d89825247d40e76086",
    "zh:a5647dcf707f7b5c154459287596ac373254ed8a1226f6071fb09df5f059f023",
    "zh:ab0294b285ece4511d1216b21efd05a619ac44e1b32c4e64aeb971924f4b5fcb",
    "zh:b66f85207e37c4e3b3159b4f1b7c9506b14fb9fb41739b2433c43e8a25118330",
    "zh:cbb44045112ed12c2c5896be38c4de14213ac30740d4a42f8f5e4aee8b056a80",
    "zh:cf6c8c1f6e8a2b572f04a274d865c0cf06edb73d89e83c1df7e3ec89b786f204",
    "zh:d8652cf37bdc1a4f84eef9f4becc6274e7285aa42fa6e4246db78042c586c584",
    "zh:f146f61103b1cd78f6b1c973c60e1e48c537bbb8d3ad126698c5e89447538ccf",
  ]
}
