# Fortigate Manager with Terraform

## Preparation
The scenario is we can do dynamic to add or remove address object only to group address, for this need to prepare
- create address group A and B

Then create terraform.auto.tfvars for credential or you can use variable input

```sh
#credential
hostname     = "xxxxxxxxxx"
username     = "xxxxxxxxxx"
password     = "xxxxxxx"
adom_name    = "xxxxxxx"
```

Request changes for ip by updating file terraform.tfvars this approach using using iprange

```sh
group_server = "group_nomadserverdtbs"
group_server = "group_nomadclientdtbs"

member_server = {
    member_nomadserverdtbs1 = {
        start_ip =  "xxxxxxxx"
        end_ip =  "xxxxxxxx"
    }
}

member_client = {
    member_nomadclientdtbs1 = {
        start_ip =  "xxxxxxxx"
        end_ip =  "xxxxxxxx"
    }
}

```


## How to run

```sh
terraform plan
then 
./apply.sh {adom_name} # this will trigger force unlock if condition is locked due to previous error

```


## How to unlock adom directly

```sh
./unlock.sh {adom_name} {presession_file} # this will trigger force unlock if condition is locked due to previous error

```

## Another example

```sh
https://gitlab.com/qomarullah/terraform-fortigate-adom
```



## Sample import command
```sh
terraform import fortimanager_object_firewall_address.member_server member_nomadserverdtbs1
terraform import fortimanager_object_firewall_address.member_client member_nomadclientdtbs1

```