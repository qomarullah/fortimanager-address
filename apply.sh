#!/bin/bash

#unlock
file="presession.txt"
adom_name=$1

if [ "$adom_name" == "" ]; then

    echo "parameter ./apply.sh {adom_name}"
    echo "example:"
    echo "./apply.sh {adom_name}"

else

    presession=`awk 1 ORS='' $file `
    echo $presession
    #apply
    dt=$(date '+%m%d%H%M%S');
    cp presession.txt presession.txt.$dt
    terraform apply -var="presession=$presession" -var="adom_name=$adom_name" --auto-approve

fi