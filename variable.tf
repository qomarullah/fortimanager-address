
variable "hostname" {
  type =  string
  default = ""
}

variable "username" {
  type =  string

}

variable "password" {
  type =  string

}

variable "presession" {
  type =  string
  default = ""
}

variable "adom_name" {
  type =  string
}

variable "member_server" {
  type =  map
  default = {}
}

variable "member_client" {
  type =  map
  default = {}
}

variable "group_server" {
  type =  string
}
variable "group_client" {
  type =  string
}

